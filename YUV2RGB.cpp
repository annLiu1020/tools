// Ann's Convert YUV to RGB 
unsigned char* RGB = new unsigned char[pixels_y];
int index = 0;
unsigned char* ybase = yuv_buffer;
unsigned char* ubase = &yuv_buffer[pixels_y];
for (int y = 0; y < DehazeConfig.imgHeight; y++) {
	for (int x = 0; x < DehazeConfig.imgWidth; x++) {
		unsigned char Y = ybase[x + y * DehazeConfig.imgWidth];
		unsigned char U = ubase[y / 2 * DehazeConfig.imgWidth + (x / 2) * 2];
		unsigned char V = ubase[y / 2 * DehazeConfig.imgWidth + (x / 2) * 2 + 1];

		unsigned char R = Y + 1.402 * (V - 128); //R
		unsigned char G = Y - 0.34413 * (U - 128) - 0.71414 * (V - 128); //G
		unsigned char B = Y + 1.772 * (U - 128); //B
		RGB[index] = std::min(std::min(R, G), B);
		index++;
	}
}
