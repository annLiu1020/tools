# $python crop_roi_comparison.py 
# usage: compare multiple results with the same ROI crop and save the ROIs.

import cv2 
import numpy as np
import os 

if __name__ = '__main__': 
    
    # list all comparison sets name 
    METHOD_NAME = ["ours", "p40_Pro-frames", "reno4-normal-frames", "reno4-night-vision1.0-frames"] 
    # image name or frame no.
    FRAME_NO = 15 
    
    for i, method in enumerate(METHOD_NAME):
        
        img_path = os.path.join(method, "IMG"+str(FRAME_NO)+".png")
        print(img_path+" processing --- ")
        
        # read input for crop the same ROI
        im = cv2.imread(img_path)
        
        if i == 0:
            r = cv2.selectROI(im)
        
        # ROI 
        imCrop = im[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+rp[2])]
        # Save ROI
        cv2.imwrite(method+str(FRAME_NO)+".png", imCrop)
        cv2.waitKey(0)